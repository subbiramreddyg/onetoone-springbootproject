package com.springdatabase.onetoone.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springdatabase.onetoone.Entity.User;



@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
