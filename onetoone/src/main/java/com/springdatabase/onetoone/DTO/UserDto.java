package com.springdatabase.onetoone.DTO;

public class UserDto {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
