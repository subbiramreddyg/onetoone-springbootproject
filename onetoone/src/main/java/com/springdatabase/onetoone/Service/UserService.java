package com.springdatabase.onetoone.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springdatabase.onetoone.Entity.User;
import com.springdatabase.onetoone.Repository.UserRepository;


@Service
public class UserService {
	@Autowired
	private UserRepository repository;
	
	public User saveUser(User user) {
		user = repository.save(user);
		return user;
	}
	public List<User> saveUsers(List<User> users) {
		return repository.saveAll(users);
	}
	public List<User> getUsers() {
		return repository.findAll();
	}
	public User getUsersById(Long userId) {
		User user = repository.findById(userId).orElse(null);
		return user;
	}
	public String deleteProduct(Long id) {
		repository.deleteById(id);
		return "User ID "+ id + " removed ";
	}
	
	
}