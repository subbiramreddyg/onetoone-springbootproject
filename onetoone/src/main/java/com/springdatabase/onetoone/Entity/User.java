package com.springdatabase.onetoone.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class User {
	@Id
	@SequenceGenerator(
			name="user_seq",
			sequenceName="user_seq",
			allocationSize=1
			)
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="user_seq"
			)
	private long id;
	private String name;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(
			name="qualification_id",
			referencedColumnName="qid"
			)
	private Qualification qualification;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(
			name="address_id",
			referencedColumnName="aid"
			)
	private Address address;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Qualification getQualification() {
		return qualification;
	}
	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	

}
