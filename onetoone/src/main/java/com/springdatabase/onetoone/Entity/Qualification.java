package com.springdatabase.onetoone.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Qualification {
	@Id
	@SequenceGenerator(
			name="qual_seq",
			sequenceName="qual_seq",
			allocationSize=1
			)
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="qual_seq"
			)
private long qid;
private String qdetails;
public long getQid() {
	return qid;
}
public void setQid(long qid) {
	this.qid = qid;
}
public String getQdetails() {
	return qdetails;
}
public void setQdetails(String qdetails) {
	this.qdetails = qdetails;
}


}
